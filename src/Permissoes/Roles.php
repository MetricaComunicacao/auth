<?php

namespace Auth\Permissoes;

use Zend\Permissions\Acl\Acl;
use Zend\Permissions\Acl\Role\GenericRole;
class Roles
{
    public function __construct(Acl $acl)
    {
        $acl->addRole(new GenericRole('superadmin'));
        $acl->addRole(new GenericRole('guest'));
        $acl->addRole(new GenericRole('usuario'),'guest');
        $acl->addRole(new GenericRole('admin'),'usuario');       
        
    }
}
