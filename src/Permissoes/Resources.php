<?php

namespace Auth\Permissoes;

use Zend\Permissions\Acl\Acl;
use Zend\Permissions\Acl\Resource\GenericResource;
class Resources
{
    public function __construct(Acl $acl)
    {
        $acl->addResource(new GenericResource(\Auth\Controller\AuthController::class));
        $acl->addResource(new GenericResource(\Application\Controller\IndexController::class));
        $acl->addResource(new GenericResource(\Painel\Controller\IndexController::class));
        $acl->addResource(new GenericResource(\Usuarios\Controller\UsuariosController::class));
        $acl->addResource(new GenericResource(\Noticias\Controller\NoticiasController::class));
        $acl->addResource(new GenericResource(\Noticias\Controller\CategoriasController::class));
    }
}
