<?php

return [
    'guest'=> array(
        'home',
        'resources',
        'roles'
    ),
    'usuario'=> array(
        'clientes'
    ),
    'admin'=> array(
        'usuarios',
        'add-user',
        'delete-user'
    ),
];
