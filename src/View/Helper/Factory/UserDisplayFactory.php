<?php

namespace Auth\View\Helper\Factory;
use Interop\Container\ContainerInterface;
use Zend\Authentication\AuthenticationServiceInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Auth\View\Helper\UserDisplay;

class UserDisplayFactory {
    public function __invoke(ContainerInterface $container)
    { 
        $authService = $container->get(AuthenticationServiceInterface::class);
        return new UserDisplay($authService);
    }
}
