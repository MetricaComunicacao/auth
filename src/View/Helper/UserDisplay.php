<?php

namespace Auth\View\Helper;
use Zend\View\Helper\AbstractHelper;

class UserDisplay extends AbstractHelper {
    private $authservice;
    private $identify;
    
    public function __construct($authservice) {
        $this->authservice = $authservice;
        $identify = $this->authservice->getIdentity();
        $this->identify = $identify["user"];
    }
    
    public function getDisplayName(){
        return $this->identify->displayname;
    }
    public function getUserName(){
        return $this->identify->username;
    }
    public function getUserRole(){
        $roles = [1 => "Admin", 2 => "Usuario", 3 => "Convidado", 5 => "Desenvolvedor"];
        return $roles[$this->identify->role];
    }
}
